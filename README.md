# tw2toot

Python script to replicate tweets in Mastodon

No need of twitter account. It scrapes the user's timeline to obtain the last 20 tweets. A Mastodon account is required.

Usage from the terminal:

./tw2toot.py -i MASTODON_INSTANCE -u MASTODON_EMAIL -p MASTODON_PASSWORD -th TWITTER_HANDLE_TO_REPLICATE &

Tips:

- Send the script to the background ("&" at the end). It will run every 5 minutes.
- Add it as a cron job when the computer starts.
