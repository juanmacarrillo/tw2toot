#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Juan M. Carrillo
"""

from pathlib import Path
import sqlite3
import requests
from bs4 import BeautifulSoup as bs
from mastodon import Mastodon
import re
import pandas as pd
import time
import argparse


# Obtain credentials and twitter handle from terminal
parser = argparse.ArgumentParser(description = "Usage example: \
                                 tw2toot.py -i https://todon.nl -u user@mail.com -p hunter11 -th realDonaldTrump")
requiredNamed = parser.add_argument_group('required named arguments')
requiredNamed.add_argument("-i", "--instance",
                    help = "Mastodon instance (e.g. https://todon.nl)", required=True)
requiredNamed.add_argument("-u", "--user",
                    help = "Mastodon user email", required=True)
requiredNamed.add_argument("-p", "--password",
                    help = "Mastodon user password", required=True)
requiredNamed.add_argument("-th", "--handle",
                    help = "Twitter handle to replicate", required=True)
args = parser.parse_args ()

mastodon_instance = args.instance
mastodon_user = args.user
mastodon_password = args.password
handle = args.handle

if not Path("pytooter_clientcred.secret").is_file():
    Mastodon.create_app(
            'tw2toot',
            api_base_url = mastodon_instance,
            to_file = 'pytooter_clientcred.secret'
            )
    
mastodon = Mastodon(
    client_id = 'pytooter_clientcred.secret',
    api_base_url = mastodon_instance
)

mastodon.log_in(
    mastodon_user,
    mastodon_password,
    to_file = 'pytooter_usercred.secret'
)

url = 'https://twitter.com/'

def tooting (new_tweets):
    # Function to publish new tweets from the database
        
        for row in range (len (new_tweets)):
            if row <= len (new_tweets):
                time.sleep (30)
                
                if handle.lower() == new_tweets['creator_handle'][row].lower():
                    msn = ('\"\"\n' +  re.sub ("pic.twitter.com/.*", "", new_tweets['tw'][row]) +  '\n\"\"\n\n' + new_tweets['path'][row] + '\n' + new_tweets['time'][row])
                else:
                    msn = ('RT: **' + new_tweets['creator'][row] + '** (@' + new_tweets['creator_handle'][row] + ')\n\n\"\"\n' +  re.sub ("pic.twitter.com/.*", "", new_tweets['tw'][row]) +  '\n\"\"\n\n' + new_tweets['path'][row] + '\n' + new_tweets['time'][row])
                
                if new_tweets['imageURL'][row] != '':
                    image = requests.get (new_tweets['imageURL'][row])
                    mast_image = mastodon.media_post (image.content, mime_type = image.headers.get ('content-type'))
                    mastodon.status_post (msn, media_ids = mast_image)
                else:
                    mastodon.status_post (msn)
            else:
                return;

        
path =  ("./" + handle + ".db")

# It creates a database with two tables, one with every tweet and the other with
# new tweets
sql = sqlite3.connect (path)
db = sql.cursor ()
db.execute ('''CREATE TABLE IF NOT EXISTS all_tweets (
       twID INT NOT NULL PRIMARY KEY,
       time VARCHAR,
       tw BLOB,
       path VARCHAR UNIQUE,
       user VARCHAR,
       creator_handle VARCHAR,
       creator VARCHAR,
       imageURL VARCHAR
        );''')

db.execute ('''CREATE TABLE IF NOT EXISTS new_tweets (
       twID INT NOT NULL default '0' PRIMARY KEY,
       time VARCHAR,
       tw BLOB,
       path VARCHAR UNIQUE,
       user VARCHAR,
       creator_handle VARCHAR,
       creator VARCHAR,
       imageURL VARCHAR
        );''')

db.execute ('''INSERT INTO all_tweets (twID)
SELECT '0'
WHERE NOT EXISTS (SELECT * FROM all_tweets)
''')
sql.commit ()

def main (handle):
    # Function to parse the Twitter page of the given handle
    # It adds the new tweets to the db and select the new ones to toot them.
    r = requests.get (url + handle)
    
#    print ("Cooking soup:", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    soup = bs (r.content, 'html.parser')
    
    tw = []
    tw_id = []
    tw_user = []
    tw_path = []
    tw_creator_handle = []
    tw_creator = []
    tw_time = []
    tw_image = []
    for all_tw in soup.find_all ('div', 'tweet'):
        
        if all_tw.find ('div', 'js-adaptive-photo'):
            tw_image.append(all_tw.find ('div', 'js-adaptive-photo')['data-image-url'])
            
        elif all_tw.find ('div', 'PlayableMedia-player'):
            tw_image.append(re.sub (".*url\(\'|\'\)", "", all_tw.find ('div', 'PlayableMedia-player') ['style']))
                
        else:
            tw_image.append ("")
            
        tw.append(all_tw.find ('p', class_ = 'tweet-text').text)
        tw_id.append(all_tw ['data-tweet-id'])
        tw_path.append('https://www.twitter.com' + all_tw ['data-permalink-path'])
        tw_creator_handle.append(all_tw ['data-screen-name'])
        tw_creator.append(all_tw ['data-name'])
        
        tw_time.append (all_tw.find ('a', 'tweet-timestamp')['title'])
        
        tw_user.append (handle)
        
        info = tw_id, tw_time, tw_user, tw, tw_path, tw_creator_handle, tw_creator, tw_image
        
        db.execute ("DELETE FROM new_tweets")
        sql.commit ()
  
        for it in range (len (info[0])):
            #print ("row: ", it)
            db.execute ('INSERT INTO new_tweets \
                        (twID, time, user, tw, path, creator_handle, creator, imageURL)\
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?)',\
                        [item[it] for item in info])
            sql.commit ()
        
        #Only new tweets and revers order (oldest first)
        newtw = pd.read_sql_query ("""
                SELECT new_tweets.*
                FROM new_tweets
                LEFT JOIN all_tweets 
                ON new_tweets.twID = all_tweets.twID
                WHERE all_tweets.twID IS NULL
                """,
                sql)
        
#        print ("Tooting:", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        tooting (newtw.iloc[::-1])
                
        # Merge all tweets
        db.execute ("""
                    INSERT OR IGNORE INTO all_tweets
                    SELECT *
                    FROM new_tweets;
                    """)
        sql.commit ()

    return;

# Runs the main function every 300 seconds (5 minutes)
# starttime = time.time ()

if __name__ == '__main__':
    while True:
        main (handle)
        #print ("Running:", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        #time.sleep (300.0 - ((time.time () - starttime) % 300.0))
